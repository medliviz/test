
# BAG Open Source Tool Template  
(Not yet fully ported !!!)
* Created by 
* Thomas Parry
* Matthias Koefferlein

## NOTES

This is a proof of concept. I substitutes Virtuoso by XSchem (for schematic entry
and parsing) and KLayout (for layout generation).

Currently it implements

* A single generator `inverter_gen`
* XSchem template, symbols
* Sky130 technology definitions (under construction)

Verification is missing yet.

Virtuoso is not required, yet in some places Virtuso concepts still are present. Removing them
is TODO.

## Instructions For Running 

Requirements:

* Linux (tested with Ubuntu 22.04)
* xschem >= 3.0, build instructions below
* Packages: git, python3, python3-pip

Setup:

```
git clone --recursive git@gitlab.com:mosaic_group/mosaic_BAG/opensource_db_template.git -b matthias-wip-sky130-pdk
cd opensource_db_template
```

Note: this already contains the `inverter_gen` submodule

Python package installation:

```
./pip3userinstall.sh
pip3 install klayout
```

Initialize the environment:

```
. opensourceme.sh
```

Then

```
./configure
```

(generates Makefiles)

```
make gen
```

Result files:

Layout: ```./inverter_generated.gds```
Generated schematic: ```inverter_generated/inverter/inverter.sch```


## XSchem 3.1 on Ubuntu

Packages

```
sudo apt-get install flex bison libx11-dev tk-dev tcl libxpm-dev
```

Building:

```
git clone https://github.com/StefanSchippers/xschem.git -b 3.1.0
cd xschem
./configure
make
make install
```


