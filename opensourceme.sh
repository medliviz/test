# Sourceme template for a BAG2 project

if [ "$PWD" = "$HOME" ]; then
 echo "Do not run this script in your home directory."
 echo "Configuration files in home directory may mess up your configuration globally."
 return 1
fi

# NOTE: added
export USER_HOME_DIR=$(pwd)/../home
export PDK_HOME=$(pwd)/sky130_pdk
export TECHLIB=sky130_fd_pr

# can be --finfet --flipwell:
export BUILDOPTS=

## Collect environment variables to the beginnning of the file
## PDK_HOME and TECHLIB are ued by BAG2 YOU MUST DEFINE THEM
##export PDK_HOME "Path to your PDK home"
##export TECHLIB "Name of the virtuoso technology library"
#####
#
#
#
#### Here I will do some checks for you so you do not screw up
if [ "$PDK_HOME" = "" ]; then
    echo "PDK_HOME not set"
    echo "In this file you MUST set your environment setup so that you have all tools available and PDK set up correctly"
    echo "It is NOT taken care for you by this file"
    echo "Exiting"
    return
fi

if [ "$TECHLIB" = "" ]; then
    echo "TECHLIB not set"
    echo "It is NOT taken care for you by this file"
    echo "Exiting"
    return
fi


#############################################################################
# NOTE: BAG_WORK_DIR, VIRTUOSO_DIR and BAG_GENERATOR_ROOT
# are decoupled for flexible definition of locations
# set BAG root directory 
#############################################################################

# Defaults to virtuoso_template root
export BAG_WORK_DIR=$(pwd)
# Virtuoso run directory from which you launch the builds 
# containing "working" artefacts, such as log files and the BAG_server_port.txt, 
# etc)
export VIRTUOSO_DIR="$BAG_WORK_DIR"
# Root directory containing generators
export BAG_GENERATOR_ROOT=${BAG_WORK_DIR}

# set BAG framework directory
export BAG_FRAMEWORK="${BAG_WORK_DIR}/BAG2_framework"
# set BAG python executable
export BAG_PYTHON="python3"
# set jupyter notebook path
#export BAG_JUPYTER "${HOME}/.local/bin/jupyter-notebook"
# set technology/project configuration directory
export BAG_TECH_CONFIG_DIR="${BAG_WORK_DIR}/BAG2_technology_definition"
# set where BAG saves temporary files
export BAG_TEMP_DIR="${VIRTUOSO_DIR}/BAGTMP"
# set location of BAG configuration file
export BAG_CONFIG_PATH="${BAG_WORK_DIR}/bag_config.yaml"

## set IPython configuration directory
##export IPYTHONDIR "$BAG_WORK_DIR/.ipython"

# This takes ecd_bag helpers into use
if [ "$PYTHONPATH" = "" ]; then
    export PYTHONPATH="${BAG_WORK_DIR}:${BAG_WORK_DIR}/bag_ecd:${BAG_WORK_DIR}/BAG2_methods:${BAG_WORK_DIR}/BAG2_framework:${BAG_WORK_DIR}/BAG2_opensource_db"
else
    export PYTHONPATH="${PYTHONPATH}:${BAG_WORK_DIR}:${BAG_WORK_DIR}/bag_ecd:${BAG_WORK_DIR}/BAG2_methods:${BAG_WORK_DIR}/BAG2_framework:${BAG_WORK_DIR}/BAG2_opensource_db"
fi

if [ ! -f ./bag_libs.def ]; then
    echo "./bag_libs.def does not exist. Creating it for you"
    echo 'BAG_prim ${BAG_TECH_CONFIG_DIR}/Tech_primitives'  > ./bag_libs.def
fi

# cds.lib still needed
if [ ! -f ./cds.lib ]; then
    echo "./cds.lib does not exist. Creating it for you"
    echo "" > ./cds.lib
fi

if [ ! -e BAGTMP ]; then
    mkdir BAGTMP
fi

# BAG_service_port still needed
if [ ! -e BAG_server_port.txt ]; then
    echo 0 >BAG_server_port.txt
fi

echo "To configure bag project for compilations, run ./configure"

