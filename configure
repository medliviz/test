#!/usr/bin/env bash
#############################################################################
# This is a configuration script to crete Berkeley Analog Generator design
# environment to an exixting and correctly defined Cadence Virtuoso design 
# environment. The purpose is to checkout all needed BAG modules 
#
# Created by Marko Kosunen on 10.10.2020
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
SCRIPTNAME="configure"
cat << EOF
${SCRIPTNAME} Release 1.0 (01.03.2020)
$(echo ${SCRIPTNAME} | tr [:upper:] [:lower:])-configure BAG design"
Written by Marko "Pikkis" Kosunen"

SYNOPSIS
$(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:])  [OPTIONS]
DESCRIPTION
    Runs configuration for the current design

OPTIONS
  -h
      Show this help.
EOF
}

while getopts h opt
do
  case "$opt" in
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

MODULES="$(sed -n '/^\s*MODULES=.*"/,/^.*"/p' init_submodules.sh \
    | sed -e 's/\\//' \
    -e 's/MODULES\s*=//' \
    -e 's/"//' \
    -e '/BAG2_framework/d' \
    -e '/BAG2_environment_settings/d' \
    -e '/BAG2_methods/d' \
    -e '/bag_ecd/d' \
    -e '/BAG2_templates/d' \
    -e'/BAG2_technology_template/d' \
    -e'/BAG2_technology_definition/d' \
    | tr  -d '\n' | sed 's/\s\s*/ /g'
)"
echo "$MODULES"

for module in ${MODULES}; do
    cd ./${module} && ./configure && cd ..
done

# Available goals
#GOALS="gen lvs drc pex"
GOALS="gen clean doc"

CURRENTFILE="./Makefile"
echo "Generating $CURRENTFILE"
cat << EOF > $CURRENTFILE
TARGETS= $(echo $MODULES | sed 's/\s\s*/ /g') 
GOALS= ${GOALS}
.PHONY: \$(TARGETS) all ${GOALS}

#Todo: need to generate recipes for other targets as well
# e.g. make gen should run gen for all modules
ifeq "\$(filter-out \$(GOALS),\$(MAKECMDGOALS))" ""
all: \$(TARGETS)

$(for target in ${GOALS}; do
echo "$target:"
for module in ${MODULES}; do
echo -e "\tcd ./$module && make $target && cd .."
done
echo ""
done
)
else
$(for target in all gen lvs drc pex clean; do
echo "$target:"
echo -e "\t@echo '"'One or more submodules given. Target '''$target''' neglected at the top'"'"
done
)
endif

$(
for module in ${MODULES}; do
echo ${module}:
echo -e "\tcd ./${module} && \\"
echo -e "\tmake \$(filter-out \$(TARGETS),\$(MAKECMDGOALS))"
done

)

EOF

# ADD Bag definitions to cds.lib
if [ "`grep cds.lib.bag cds.lib`" == "" ]; then
    sed -i '1s|^|SOFTINCLUDE ./cds.lib.bag\n|' cds.lib
fi

# Create bag_libs.def if it does not exist 
CURRENTFILE="./bag_libs.def"
if [ ! -f $CURRENTFILE ]; then
echo "Generating $CURRENTFILE"
cat << EOF > $CURRENTFILE
BAG_prim \${BAG_TECH_CONFIG_DIR}/Tech_primitives
EOF
fi

echo "Configured." 
echo "Make sure that your module templates are visible in 
Virtuoso Library manager."
exit 0


