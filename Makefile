TARGETS= inverter_gen 
GOALS= gen clean doc
.PHONY: $(TARGETS) all gen clean doc

#Todo: need to generate recipes for other targets as well
# e.g. make gen should run gen for all modules
ifeq "$(filter-out $(GOALS),$(MAKECMDGOALS))" ""
all: $(TARGETS)

gen:
	cd ./inverter_gen && make gen && cd ..

clean:
	cd ./inverter_gen && make clean && cd ..

doc:
	cd ./inverter_gen && make doc && cd ..
else
all:
	@echo 'One or more submodules given. Target all neglected at the top'
gen:
	@echo 'One or more submodules given. Target gen neglected at the top'
lvs:
	@echo 'One or more submodules given. Target lvs neglected at the top'
drc:
	@echo 'One or more submodules given. Target drc neglected at the top'
pex:
	@echo 'One or more submodules given. Target pex neglected at the top'
clean:
	@echo 'One or more submodules given. Target clean neglected at the top'
endif

inverter_gen:
	cd ./inverter_gen && \
	make $(filter-out $(TARGETS),$(MAKECMDGOALS))

