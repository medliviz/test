.. thesdk documentation master file.
   You can adapt this file completely to your liking, 
   but it should at least contain the root `toctree` directive.

=============================
MOSAIC BAG2 virtuoso template
=============================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   introduction

   bag_ecd/sections

   BAG2_technology_template/sections

   BAG2_framework/index.rst

   BAG2_templates/index.rst

   documentation_instructions

   developers_guide

   indices_and_tables


